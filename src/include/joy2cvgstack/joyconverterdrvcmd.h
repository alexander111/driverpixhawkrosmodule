#include "droneModuleROS.h"

//Input
#include "sensor_msgs/Joy.h"

//Output
#include "droneMsgsROS/droneAutopilotCommand.h"
#include "communication_definition.h"

#include "math.h"


class JoyConverterDrvOktoCmd : public DroneModule
{

    //Publisher
protected:
    ros::Publisher OutputPubl;

    //Subscriber
protected:
    ros::Subscriber InputSubs;
    void inputCallback(const sensor_msgs::Joy::ConstPtr& msg);


    //Constructors and destructors
public:
    JoyConverterDrvOktoCmd();
    ~JoyConverterDrvOktoCmd();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};



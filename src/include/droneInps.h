//////////////////////////////////////////////////////
//  parrotARDroneInps.h
//
//  Created on: Nov 12, 2015
//      Author: hriday bavle
//
//  Last modification on: Nov 19, 2015
//      Author: hriday bavle
//
//////////////////////////////////////////////////////

#include <iostream>
#include <math.h>
#include <cmath>

//// ROS  ///////
#include "ros/ros.h"

#include "droneModuleROS.h"
#include "communication_definition.h"
#include "droneMsgsROS/droneAutopilotCommand.h"
#include "droneMsgsROS/battery.h"

//geometry msgs
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"

//standard messages ROS
#include "std_msgs/Float64.h"
#include "tf/transform_datatypes.h"
#include "eigen_conversions/eigen_msg.h"
#include "Eigen/Core"
#include "Eigen/Geometry"
#include "tf_conversions/tf_eigen.h"

//Mavros
#include "mavros_msgs/AttitudeTarget.h"
#include "mavros_msgs/ActuatorControl.h"
#include "mavros_msgs/State.h"
#include "mavros/mavros_uas.h"
#include <pluginlib/class_list_macros.h>

#include <mavros/mavros_plugin.h>
#include <mavros/setpoint_mixin.h>

/////////////////////////////////////////
// Class DroneCommand
//
//   Description
//
/////////////////////////////////////////
class DroneCommandROSModule : public DroneModule
{
    //Constructors and destructors
public:
    DroneCommandROSModule();
    ~DroneCommandROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);
    void sendSetpoints();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

protected:
    //Subscribers
    ros::Subscriber ML_autopilot_command_subscriber;
    ros::Subscriber rotation_angles_subscriber;
    ros::Subscriber battery_subscriber;
    void MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg);
    void rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
    void batteryCallback(const droneMsgsROS::battery& msg);
    // Publishers
    ros::Publisher drone_CtrlInput_publisher;
    ros::Publisher drone_setpoint_attitude_throttle_publ;

public:
    std::vector<geometry_msgs::PoseStamped> poses;
    geometry_msgs::Quaternion orientation;
    Eigen::Quaterniond quaterniond, quaterniond_transformed;
    double roll_command, pitch_command, yaw_command, yaw_angle;
    double roll, pitch, yaw;
    double battery_percent;

};

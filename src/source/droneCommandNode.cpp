//////////////////////////////////////////////////////
//  DroneCommandNode.cpp
//
//  Created on: Nov 12, 2015
//      Author: hriday bavle
//
//  Last modification on: Nov 19, 2015
//      Author: hriday bavle
//
//////////////////////////////////////////////////////



//I/O stream
//std::cout
#include <iostream>


//ROS
#include "ros/ros.h"

//parrotARDrone
#include "droneInps.h"

#include "geometry_msgs/PoseStamped.h"

//Comunications
#include "communication_definition.h"

//std::thread
#include <thread>

using namespace std;

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, "droneCommand");
    ros::NodeHandle n;

    cout<<"[ROSNODE] Starting droneCommand"<<endl;

    //Vars
    DroneCommandROSModule MyDroneCommandROSModule;
    MyDroneCommandROSModule.open(n,"droneCommand");

    try
    {
        //Read messages
        ros::spin();
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}

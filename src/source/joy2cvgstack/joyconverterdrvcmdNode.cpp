//I/O stream
//std::cout
#include <iostream>

//ROS
#include "ros/ros.h"

//parrotARDrone
#include "joy2cvgstack/joyconverterdrvcmd.h"

using namespace std;

int main(int argc,char **argv)
{
    //Ros Inits
    ros::init(argc, argv,"JoyConverterDrvCmd");
    ros::NodeHandle n;

    cout<<"[ROSNODE] Starting MyJoyConverterRODModule"<<endl;

    //Vars
    JoyConverterDrvOktoCmd MyJoyConverterRODModule;
    MyJoyConverterRODModule.open(n,"JoyConverterDrvCmd");

    try
    {
        //Read messages
        ros::spin();
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}


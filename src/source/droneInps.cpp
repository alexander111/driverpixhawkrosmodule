//Drone
#include "droneInps.h"

// The commands comming from the midlevel controller needs to be muliplied by these values
// to covert the thrust commands in the range of -1 to +1
static double THRUST_SCALE_FULL_BATTERY = 1/32.681;
static double THRUST_SCALE_HALF_BATTERY = 1/29.71;
static double THRUST_SCALE_LOW_BATTERY  = 1/27.43;

////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() :
    DroneModule(droneModule::active)
{
    init();
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{
    return;
}

void DroneCommandROSModule::init()
{
    return;
}

void DroneCommandROSModule::close()
{
    return;
}

void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Publishers
    drone_CtrlInput_publisher       = n.advertise<mavros_msgs::AttitudeTarget>("mavros/setpoint_raw/attitude", 1, true);

    //Subscribers
    ML_autopilot_command_subscriber = n.subscribe("command/low_level", 1, &DroneCommandROSModule::MLAutopilotCommandCallback, this);
    rotation_angles_subscriber      = n.subscribe("rotation_angles", 1, &DroneCommandROSModule::rotationAnglesCallback, this);
    battery_subscriber              = n.subscribe("battery",1, &DroneCommandROSModule::batteryCallback, this);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void DroneCommandROSModule::MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    mavros_msgs::AttitudeTarget attitude_msg;

    //adding the current yaw angle to the dyaw command
    yaw_command = yaw_angle + msg.dyaw;

    //convert the values in radians
    roll_command  = (+1)*msg.roll*(M_PI/180.0);
    pitch_command = (+1)*msg.pitch*(M_PI/180.0);
    yaw_command   = (+1)*yaw_command*(M_PI/180.0);

    //convert euler angles to quaternion but in eigen form
    quaterniond =  mavros::UAS::quaternion_from_rpy(roll_command, pitch_command, yaw_command);

    //converting the NED frame to ENU for mavros
    quaterniond_transformed = mavros::UAS::transform_orientation_ned_enu(mavros::UAS::transform_orientation_baselink_aircraft
                                               (quaterniond));

    //convert quaternion in eigen form to geometry_msg form
    tf::quaternionEigenToMsg(quaterniond_transformed, orientation);


    attitude_msg.orientation = orientation;

    //choose the thrust values depending on the battery percentage
//    if(battery_percent <= 100.0 && battery_percent >= 50.0)
//       //converting thrust in the range of 0 to +1
//      attitude_msg.thrust = msg.thrust * THRUST_SCALE_FULL_BATTERY;
//    else if(battery_percent < 50.0 && battery_percent >= 0.0)
//      attitude_msg.thrust = msg.thrust * THRUST_SCALE_HALF_BATTERY;
//    else if(battery_percent < 0 && battery_percent >= -20.0)
//      attitude_msg.thrust = msg.thrust * THRUST_SCALE_LOW_BATTERY;
//    else
      attitude_msg.thrust = msg.thrust * THRUST_SCALE_HALF_BATTERY;

    drone_CtrlInput_publisher.publish(attitude_msg);

    return;
}

void DroneCommandROSModule::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
    yaw_angle = msg.vector.z;
    return;
}

void DroneCommandROSModule::batteryCallback(const droneMsgsROS::battery &msg)
{
    battery_percent = msg.batteryPercent;
    return;
}
